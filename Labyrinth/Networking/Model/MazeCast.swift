//
//  MazeCast.swift
//  Labyrinth
//
//  Created by Terence on 2018-11-25.
//  Copyright © 2018 Terence Yan. All rights reserved.
//

import Foundation

struct MazeCast: Codable {
    let person: MazePerson?
    let character: MazeCharacter?
}

struct MazePerson: Codable {
    let id: Int?
    let name: String?
}

struct MazeCharacter: Codable {
    let id: Int?
    let name: String?
    let url: String?
}
