//
//  MazeSchedule.swift
//  Labyrinth
//
//  Created by Terence on 2018-11-25.
//  Copyright © 2018 Terence Yan. All rights reserved.
//

import Foundation

struct MazeSchedule: Codable {
    let id: Int?
    let url: String?
    let name: String?
    let season: Int?
    let episode: Int?
    let airdate: String?
    let airtime: String?
    let duration: Int?
    let show: MazeShow?
    
    enum CodingKeys: String, CodingKey {
        case id
        case url
        case name
        case season
        case episode = "number"
        case airdate
        case airtime
        case duration = "runtime"
        case show
    }
    
    func getNormalizeTitle() -> String? {

        if let title = show?.name {
            return title
        }
//        if name?.starts(with: "Episode") == true {
//            return show?.name
//        }
        return name
    }
}

struct MazeShow: Codable {
    let id: Int?
    let url: String?
    let name: String?
    let language: String?
    let summary: String?
    let genres: Set<String>?
    let network: MazeNetwork?
    let image: [String:String]?
    let type: String?
}

struct MazeNetwork: Codable {
    let id: Int?
    let name: String?
}
