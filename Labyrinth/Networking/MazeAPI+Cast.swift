//
//  MazeAPI+Search.swift
//  Labyrinth
//
//  Created by Terence on 2018-11-25.
//  Copyright © 2018 Terence Yan. All rights reserved.
//

import Foundation
import Alamofire

fileprivate extension NetworkConstants {
    static let cast: String = "\(NetworkConstants.baseUrl)/shows/$0/cast"
}

extension MazeAPI {
    func getCast(for showId: Int, completion: @escaping ([MazeCast]) -> Void) {
        
        let resource = NetworkConstants.cast.replacingOccurrences(of: "$0", with: "\(showId)")
        performRequest(to: resource, with: nil, successHandler: completion) { (error) in
            print(error.localizedDescription)
        }
    }
}
