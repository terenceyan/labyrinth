//
//  MazeAPI.swift
//  Labyrinth
//
//  Created by Terence on 2018-11-25.
//  Copyright © 2018 Terence Yan. All rights reserved.
//

import Foundation
import Alamofire

internal struct NetworkConstants {
    static let baseUrl: String = "https://api.tvmaze.com"
}

enum MazeError: Error {
    case EmptyResponse
    case Serialization
    case Encoding
}

class MazeAPI: NSObject {
    internal func performRequest<T:Codable>(to resource: String, with params: Parameters?, successHandler: @escaping (T) -> Void, errorHandler: @escaping (Error) -> Void) {
        Alamofire.request(resource, method: .get, parameters: params, encoding: URLEncoding.queryString, headers: nil)
            .validate(statusCode: 200..<300).validate(contentType: ["application/json"])
            .responseString { response in
                guard let result = response.result.value else {
                    errorHandler(MazeError.EmptyResponse)
                    return
                }
                
                guard let data = result.data(using: String.Encoding.utf8) else {
                    errorHandler(MazeError.Encoding)
                    return
                }
                
                do {
                    let decoder = JSONDecoder()
                    let model: T = try decoder.decode(T.self, from: data)
                    successHandler(model)
                } catch let error {
                    print(error.localizedDescription)
                    errorHandler(MazeError.Serialization)
                    return
                }
        }
    }
}


