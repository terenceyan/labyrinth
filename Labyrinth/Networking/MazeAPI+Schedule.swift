//
//  MazeAPI+Schedule.swift
//  Labyrinth
//
//  Created by Terence on 2018-11-25.
//  Copyright © 2018 Terence Yan. All rights reserved.
//

import Foundation

fileprivate extension NetworkConstants {
    static let schedule: String = "\(NetworkConstants.baseUrl)/schedule"
}

extension MazeAPI {
    func getSchedule(for date: Date, completion: @escaping ([MazeSchedule]) -> Void) {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        
        let day = formatter.string(from: date)
        let params: [String: Any] = ["country":"US", "date": day]
        
        // FIXME: - Deduplicate repeating shows that sporadically occur on date boundary...🤦‍♂️
        performRequest(to: NetworkConstants.schedule, with: params, successHandler: completion) { (error) in
            print(error.localizedDescription)
        }
    }
}




