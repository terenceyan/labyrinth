//
//  AppImageCache.swift
//  Labyrinth
//
//  Created by Terence on 2018-11-25.
//  Copyright © 2018 Terence Yan. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

// Quick and dirty image cache
class AppImageCache: NSObject {
    static let instance: AppImageCache = AppImageCache()
    
    let cache: NSCache<NSString, UIImage>
    
    override init() {
        cache = NSCache()
        cache.countLimit = 100
        super.init()
    }
    
    func fetchImage(for url:String, completion: @escaping (UIImage) -> Void) {
        
        // TVMaze can serve https but resp data urls are still (╯°□°）╯︵ ┻━┻
        let imageUrl: String = fixAtsCompliance(imageUrl: url)
        
        let key: NSString = NSString(string: imageUrl)
        if let cachedImage = cache.object(forKey: key) {
            completion(cachedImage)
            return
        }
        
        Alamofire.request(imageUrl).validate(statusCode: 200..<300).validate(contentType: ["image/jpeg"]).responseData { (response) in
            if response.result.isSuccess,
                let data = response.data,
                let image = UIImage(data: data) {
                self.cache.setObject(image, forKey: key) // hmmmm
                completion(image)
            } else {
                // TODO: - What to do when url isn't accessible on TLS1_2? -> Proxy the fetch through a dummy njs function?
            }
        }
    }
    
    

    fileprivate func fixAtsCompliance(imageUrl: String) -> String {
        if imageUrl.starts(with: "http:"),
            var url = URLComponents(string: imageUrl) {
            url.scheme = "https"
            return url.string ?? imageUrl
        }
        return imageUrl
    }
    
}
