//
//  DetailViewController.swift
//  Labyrinth
//
//  Created by Terence on 2018-11-25.
//  Copyright © 2018 Terence Yan. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet var imagePoster: UIImageView!
    @IBOutlet var seasonLabel: UILabel!
    @IBOutlet var episodeLabel: UILabel!
    @IBOutlet var genreLabel: UILabel!
    @IBOutlet var durationLabel: UILabel!
    @IBOutlet var castNames: UILabel!
    @IBOutlet var summaryBlock: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    
    
    
    func configureView() {
        // Update the user interface for the detail item.
        if let detail = detailItem {
            
            if let imageUrl = detail.show?.image?["original"] ?? detail.show?.image?["medium"] {
                imagePoster.setCachedImage(from: imageUrl)
            }
            
            seasonLabel.text = "Season \(detail.season ?? 0)"
            if let episode = detail.episode {
                episodeLabel.text = "Episode \(episode)"
            } else {
                episodeLabel.text = "Unknown episode"
            }
            
            if let genres = detail.show?.genres {
                genreLabel.text = genres.joined(separator: ", ")
            } else {
                genreLabel.text = detail.show?.type
            }
            
            if let mins = detail.duration {
                durationLabel.text = "\(mins) mins"
            }
            
            setCastLabel(detail)
            
            setSummaryLabel(detail)
            
            scrollView.isHidden = false
            
        } else {
            scrollView.isHidden = true
        }
    }
    
    fileprivate func setSummaryLabel(_ detail: MazeSchedule) {
        if let summary = detail.show?.summary,
            let data = summary.data(using: String.Encoding.utf8),
            let summaryText = try? NSMutableAttributedString(data: data, options: [NSMutableAttributedString.DocumentReadingOptionKey.documentType: NSMutableAttributedString.DocumentType.html], documentAttributes: nil) {
            
            let fullRange = NSRange(location: 0, length: summaryText.length)
            
            // Set it back to right font and color
            let attributes: [NSAttributedString.Key: Any] = [
                .font: UIFont.systemFont(ofSize: 14),
                .foregroundColor: UIColor.lightGray
            ]
            summaryText.addAttributes(attributes, range: fullRange)
            
            summaryBlock.attributedText = summaryText
            
        }
    }
    
    fileprivate func setCastLabel(_ detail: MazeSchedule) {
        MazeAPI().getCast(for: (detail.show?.id)!) { (casts) in
            if casts.count == 1 {
                self.castNames.text = casts[0].person?.name
            } else if casts.count == 0 {
                self.castNames.text = "N/A" // Oh bad data
            } else {
                let maxIndex = casts.count < 10 ? casts.count : 10
                self.castNames.text = casts[0..<maxIndex].map({ (cast) -> String in
                    cast.person!.name!
                }).joined(separator: ", ")
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        configureView()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    var detailItem: MazeSchedule?
}

