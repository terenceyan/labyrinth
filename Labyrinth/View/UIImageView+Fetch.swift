//
//  UIImageView+Fetch.swift
//  Labyrinth
//
//  Created by Terence on 2018-11-25.
//  Copyright © 2018 Terence Yan. All rights reserved.
//

import Foundation
import UIKit

extension UIImageView {
    func setCachedImage(from url: String) {
        AppImageCache.instance.fetchImage(for: url) { (image) in
            self.image = image
        }
    }
}
