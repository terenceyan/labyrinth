//
//  MasterViewController.swift
//  Labyrinth
//
//  Created by Terence on 2018-11-25.
//  Copyright © 2018 Terence Yan. All rights reserved.
//

import UIKit

class MasterViewController: UITableViewController, UISearchBarDelegate, UISearchControllerDelegate {

    var detailViewController: DetailViewController? = nil
    let searchController = UISearchController(searchResultsController: nil)
    
    var objects = [Int:[MazeSchedule]]()
    var filteredObjects: [Int:[MazeSchedule]]? = nil
    
    var baseDate: Date? // for the late night tv viewers
    
    fileprivate let reuseIdentifier = "ScheduleEventCell" // TODO: Replace with enum
    fileprivate let headerResuseIdentifier = "ScheduleEventHeader"
    fileprivate let sectionDateFormatter = DateFormatter()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.

        let cellNib = UINib(nibName: "ScheduleEventCellTableViewCell", bundle: nil)
        self.tableView.register(cellNib, forCellReuseIdentifier: reuseIdentifier)

        if let split = splitViewController {
            let controllers = split.viewControllers
            detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? DetailViewController
            splitViewController?.displayModeButtonItem.title = "Schedule"
        }
        
        configureSearchController()
        
        baseDate = findStartDate()
        
        sectionDateFormatter.locale = Locale(identifier: "en_US")
        sectionDateFormatter.dateStyle = .full
        
        fetchToToday()
    }
    
    fileprivate func configureSearchController() {
        definesPresentationContext = true
        navigationItem.searchController = searchController
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Find Shows"
        searchController.searchBar.delegate = self
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    

    // MARK: - Segues

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            if let indexPath = tableView.indexPathForSelectedRow {
                let object = (filteredObjects ?? objects)[indexPath.section]?[indexPath.row]
                let controller = (segue.destination as! UINavigationController).topViewController as! DetailViewController
                controller.detailItem = object
                controller.navigationItem.title = object?.getNormalizeTitle()
                controller.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
                controller.navigationItem.leftItemsSupplementBackButton = true
            }
        }
    }

    // MARK: - Table View

    override func numberOfSections(in tableView: UITableView) -> Int {
        return (filteredObjects ?? objects).count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (filteredObjects ?? objects)[section]!.count
    }
    

    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: headerResuseIdentifier) ??
                UITableViewHeaderFooterView(reuseIdentifier: "ScheduleEventHeader")
            header.textLabel?.text = sectionDateFormatter.string(from: baselineDate(after: section))
            header.textLabel?.font = UIFont.preferredFont(forTextStyle: .body) // Need to set font to overwrite for color to set correctly.
            header.textLabel?.textColor = UIColor.white
        
            header.contentView.backgroundColor = UIColor.darkGray
            return header
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as? ScheduleEventCellTableViewCell ?? ScheduleEventCellTableViewCell(style: .default, reuseIdentifier: reuseIdentifier)
        
        let data = filteredObjects ?? objects
        if let object = data[indexPath.section]?[indexPath.row] {
            cell.title.text = object.getNormalizeTitle()
            cell.time.text = object.airtime
            cell.date.text = object.airdate
            
            if let network = object.show?.network?.name {
                cell.network.text = "on \(network)"
            }
            if let season = object.season,
                let episode = object.episode {
                cell.episode.text = String.init(format: "S%02dE%02d", season, episode)
            }
            
            // Only grande for everyone?
            if let imageUrl = object.show?.image?["medium"] ?? object.show?.image?["original"] {
                cell.poster.setCachedImage(from: imageUrl)
            }
            
        } else {
            print("The impossible has happened. 🐷 are flying")
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if (filteredObjects ?? objects)[indexPath.section]?[indexPath.row] != nil {
            self.performSegue(withIdentifier: "showDetail", sender: nil)
        }
    }
    
    // MARK: - Search
    
    @IBAction func searchButtonTapped(_ sender: UIBarButtonItem) {
        searchController.searchBar.becomeFirstResponder() // Must be called before setting search controller active... 😒
        searchController.isActive = true
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard (searchController.searchBar.text?.isEmpty == false) && searchController.isActive else {
            filteredObjects = nil
            return
        }
        
        guard let keywords = searchController.searchBar.text else {
            return
        }
        
        filterObjects(keywords)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        filteredObjects = nil
        self.tableView.reloadData()
    }
    
    fileprivate func filterObjects(_ keywords: String) {
        var filterCopy = [Int:[MazeSchedule]]()
        for index in 0...6 {
            if let daySchedule = objects[index] {
                let filteredDay = daySchedule.filter { (schedule) -> Bool in
                    return (schedule.name?.contains(keywords) == true || schedule.show?.network?.name?.localizedCaseInsensitiveContains(keywords) == true)
                }
                
                filterCopy[index] = filteredDay
            }
        }
        
        filteredObjects = filterCopy
        self.tableView.reloadData()
    }

    
    // MARK: - Fetch
    func fetchToToday() {
        let calendar = Calendar(identifier: .gregorian)

        // TODO: - Implement prefetch datasource
        for index in 0...6 {
            let fetchDate = calendar.date(byAdding: .day, value: index, to: baseDate!)!
            MazeAPI().getSchedule(for: fetchDate) { (schedules) in
                self.objects[index] = schedules
                if self.objects.count == 7 {
                    self.tableView.reloadData()
                }
            }
        }
        
    }

    
    // MARK: - Those Helpful Things
    fileprivate func findStartDate() -> Date {
        let calendar = Calendar(identifier: .gregorian)
        let startDate = Date()
        
        var weekComponent = calendar.dateComponents([.year, .weekOfYear], from: startDate)
        
        // On Sunday, the previous gregorian week should be fetched because 'week' starts on 'Monday'
        if calendar.dateComponents([.weekday], from: startDate).weekday == 1 {
            if weekComponent.weekOfYear == 1 { // First sunday of a year
                weekComponent.year = weekComponent.year! - 1
                weekComponent.weekOfYear = 52
            }
            weekComponent.weekOfYear = weekComponent.weekOfYear! - 1
        }
        
        weekComponent.weekday = 2
        return calendar.date(from: weekComponent)!
    }
    
    fileprivate func baselineDate(after days: Int) -> Date {
        return baseDate!.addingTimeInterval(TimeInterval(86400 * days)) // FIXME: - To work with non-gregorian calendars?
    }

}

